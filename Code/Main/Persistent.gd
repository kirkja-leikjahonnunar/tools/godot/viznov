#extends Node
# Singelton class Persistent: `Project` > `Project Settings...` > `Autoload` tab > `Persistent`.
class_name Persistent

const SAVE_FILE: String = "user://viznov.dat"

static var data: Dictionary = {
	"speaker_data" : {
		"who" : "óverðtryggt",
		"says" : "Kvað seigir þú?",
		"choices" : ["Ég seigi allt gott.", "Ég seigi ekki allt gott."],
		"tint" : Color.BLACK
		}
	}


static func Load():
	print("Loading from: ", SAVE_FILE)
	var file: FileAccess = FileAccess.open(SAVE_FILE, FileAccess.READ)
	
	# If the file isn't there...
	if not file:
		print("No save file found... creating new save file at: ", SAVE_FILE)
		Save() # ...make a new save file with the default `data`.
		
		# Attempt to read again.
		file = FileAccess.open(SAVE_FILE, FileAccess.READ)
		
		# If the file still isn't there...
		if not file:
			push_error(FileAccess.get_open_error())
			push_error("Unable to create new save file. Do we have folder permissions?")
			return # ...push errors and return.
	
	# Grab the data from the JSON file.
	var json_text = file.get_as_text()
	var json_data = JSON.parse_string(json_text)

	# Returns null if parsing failed.
	if json_data:
		Persistent.data = json_data
	else:
		push_error("File text data failed to parse.")


static func Save():
	print("Saving to: ", SAVE_FILE)
	var file = FileAccess.open(SAVE_FILE, FileAccess.WRITE)
	
	# If the file isn't there...
	if not file:
		push_error(FileAccess.get_open_error())
		push_error("Unable to write to this location. Do we have folder permissions?")
		return # ...push errors and return.
	
	file.store_string(JSON.stringify(Persistent.data))



# The main player data to save.
# = {
#	"player": {
#		"name": "óverðtryggt",
#		"language": "is",
#		"symptoms": ["body attack", "runny nose"],
#		"moods": [],
#		"dates":
#			[{
#				"dict": {"year": 2023, "month": 3, "day": 1},
#				"symptoms":
#					[	# Index referencing the `symptoms[]` array.
#						{"index": 0, "magnitude": 3},
#						{"index": 1, "magnitude": 1},
#						{"index": 2, "magnitude": 3}
#					],
#				"moods":
#					[	# Index referencing the `moods[]` array.
#						{"index": 0, "magnitude": 0},
#						{"index": 1, "magnitude": 2},
#						{"index": 2, "magnitude": 0},
#						{"index": 3, "magnitude": 2},
#						{"index": 4, "magnitude": 0},
#						{"index": 5, "magnitude": 2},
#						{"index": 6, "magnitude": 0},
#						{"index": 7, "magnitude": 2},
#						{"index": 8, "magnitude": 0},
#						{"index": 9, "magnitude": 2},
#						{"index": 10, "magnitude": 0},
#						{"index": 11, "magnitude": 2},
#						{"index": 12, "magnitude": 0},
#						{"index": 13, "magnitude": 2},
#						{"index": 14, "magnitude": 0},
#						{"index": 15, "magnitude": 2},
#						{"index": 16, "magnitude": 0},
#						{"index": 17, "magnitude": 2},
#						{"index": 18, "magnitude": 0},
#						{"index": 19, "magnitude": 2},
#						{"index": 20, "magnitude": 0},
#						{"index": 21, "magnitude": 2},
#						{"index": 22, "magnitude": 0},
#						{"index": 23, "magnitude": 2},
#						{"index": 24, "magnitude": 2},
#						{"index": 25, "magnitude": 0},
#						{"index": 26, "magnitude": 2},
#						{"index": 27, "magnitude": 0},
#						{"index": 28, "magnitude": 2},
#						{"index": 29, "magnitude": 0},
#					]
#			},
#			{
#				"dict": {"year": 2023, "month": 3, "day": 4},
#				"symptoms":
#					[	# Index referencing the `symptoms[]` array.
#						{"index": 0, "magnitude": 3},
#						{"index": 4, "magnitude": 1},
#						{"index": 3, "magnitude": 3}
#					],
#				"moods":
#					[	# Index referencing the `moods[]` array.
#						{"index": 2, "magnitude": 0},
#						{"index": 3, "magnitude": 2}
#					]
#			},
#			{
#				"dict": {"year": 2023, "month": 2, "day": 14},
#				"symptoms": [],
#				"moods":
#					[	# Index referencing the `moods[]` array.
#						{"index": 1, "magnitude": 0},
#						{"index": 3, "magnitude": 2}
#					]
#			},
#			{
#				"dict": {"year": 2023, "month": 3, "day": 5},
#				"symptoms":
#					[	# Index referencing the `moods[]` array.
#						{"index": 0, "magnitude": 1}
#					],
#				"moods":
#					[	# Index referencing the `moods[]` array.
#						{"index": 0, "magnitude": 2},
#						{"index": 3, "magnitude": 1}
#					]
#			}]
#		},
#	}

# Merges the player's custom data into the existing
# Persistent.symptoms and Persistent.moods.
#static func MergeData(data: Dictionary):
#	# Calling the class directly works.
#	Persistent.symptoms.append_array(data.player.symptoms)
#	Persistent.moods.append_array(data.player.moods)
#	print("SIMPS: ", Persistent.symptoms)
#	print("MOODS: ", Persistent.moods)
