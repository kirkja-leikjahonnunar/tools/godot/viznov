extends TextEdit
class_name ChoiceUI

@onready var close_button = $CloseButton
@export var speaker_node: SpeakerNode


func _ready():
	mouse_entered.connect(OnMouseEntered)
	close_button.mouse_entered.connect(OnMouseEntered)
	
	mouse_exited.connect(OnMouseExited)
	close_button.mouse_exited.connect(OnMouseExited)
	
	close_button.pressed.connect(OnClosePressed)
	close_button.hide()


func OnMouseEntered():
	close_button.show()
#	print("in")


func OnMouseExited():
	close_button.hide()
#	print("out")

func OnClosePressed():
	speaker_node.RemoveChoice(self)
