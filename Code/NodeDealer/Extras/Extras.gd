extends PanelContainer
class_name ExtrasPanel

@onready var transition_options = $Grid/TransitionOptions
@onready var close_button = $Grid/CloseButton

@export var open_time: float = 0.3
@export var close_time: float = 0.2

var node_dealer: NodeDealer


func _ready():
	hide()
	close_button.pressed.connect(Close)
	
	# Transition options for the dealing motion graphics.
	transition_options.add_item("linear")
	transition_options.add_item("sine")
	transition_options.add_item("quintic")
	transition_options.add_item("quartic")
	transition_options.add_item("quadratic")
	transition_options.add_item("exponential")
	transition_options.add_item("elastic")
	transition_options.add_item("cubic")
	transition_options.add_item("square root")
	transition_options.add_item("bounce")
	transition_options.add_item("back out")
	transition_options.add_item("spring")
	Close()


func Open():
	var tween = create_tween()
	tween.tween_callback(show)
	tween.set_ease(Tween.EASE_OUT)
	tween.set_trans(Tween.TRANS_BACK)
	tween.tween_property(self, "scale", Vector2.ONE, 0.3)


func Close():
	var tween = create_tween()
	tween.set_parallel(false)
	tween.set_ease(Tween.EASE_IN)
	tween.set_trans(Tween.TRANS_BACK)
	tween.tween_property(self, "scale", Vector2(0.1, 0.1), 0.15)
	tween.tween_callback(hide)



func OnItemSelected(index: int):
	node_dealer.transition_type = index
#	match index:
#		0:
#			option_button.tooltip_text = option_button.get_item_tooltip(index)
#● TRANS_LINEAR = 0
#The animation is interpolated linearly.
#● TRANS_SINE = 1
#The animation is interpolated using a sine function.
#● TRANS_QUINT = 2
#The animation is interpolated with a quintic (to the power of 5) function.
#● TRANS_QUART = 3
#The animation is interpolated with a quartic (to the power of 4) function.
#● TRANS_QUAD = 4
#The animation is interpolated with a quadratic (to the power of 2) function.
#● TRANS_EXPO = 5
#The animation is interpolated with an exponential (to the power of x) function.
#● TRANS_ELASTIC = 6
#The animation is interpolated with elasticity, wiggling around the edges.
#● TRANS_CUBIC = 7
#The animation is interpolated with a cubic (to the power of 3) function.
#● TRANS_CIRC = 8
#The animation is interpolated with a function using square roots.
#● TRANS_BOUNCE = 9
#The animation is interpolated by bouncing at the end.
#● TRANS_BACK = 10
#The animation is interpolated backing out at ends.
#● TRANS_SPRING = 11
#The animation is interpolated like a spring towards the end.

#	for i in 12:
#		option_button.add_item(str(i))
