extends Button
class_name ChoiceButton

signal ahoy(index: int)

var index: int


func _ready():
	pressed.connect(OnPressed)


func OnPressed():
	ahoy.emit(index)
