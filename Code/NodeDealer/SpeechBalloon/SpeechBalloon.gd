extends PanelContainer
class_name SpeechBalloon

const CHOICE_BUTTON = preload("res://NodeDealer/SpeechBalloon/ChoiceButton/ChoiceButton.tscn")

@onready var v_box: VBoxContainer = $VBox
@onready var says_ui: Label = $VBox/SaysUI
@onready var button_choices: VBoxContainer = $VBox/ButtonChoices

var node_dealer: NodeDealer ## Spawner reference.
var data: SpeakerData ## Data structure.

# Tween and times.
var tween: Tween
var quickly: float = 0.2
var opening_time: float = 0.6
var closing_time: float = 0.3
var reading_time: float = 0.5


func _ready():
	self.hide()


## Opens the `SpeechBalloon` with tweening animation.
func Open(new_data: SpeakerData):
	if new_data:
		data = new_data
		node_dealer.name_tag.text = data.who
		says_ui.text = data.says
		
		Dev.RemoveChildrenOf(button_choices)
		var choice_index: int = 0
		for choice_text in data.choices:
			SpawnChoice(choice_index, choice_text)
			choice_index += 1
	else:
		# HACK: How should we handle a deadend?
		print_debug("Null Data.")
		node_dealer.name_tag.text = "M.C.P."
		says_ui.text = "End of line."
		Dev.RemoveChildrenOf(button_choices)
#		SpawnChoice(0, "Quit")
	
	TweenOpen(opening_time)


func SpawnChoice(index: int, text: String):
	var choice = CHOICE_BUTTON.instantiate()
	button_choices.add_child(choice)
	choice.index = index
	choice.text = text
	choice.tooltip_text = "Index: " + str(choice.index)
	choice.ahoy.connect(OnAhoyed)


func OnAhoyed(index: int):
	var data = node_dealer.dialog_tree.GetSpeakerData(index)
	print("OnAhoyed(): ", index, ", data: ", data)
	Open(data)


## Animation tween for the `Open()` function.
func TweenOpen(duration: float):
	# Set initial properties before showing.
	says_ui.visible_ratio = 0.0
	pivot_offset = Vector2(0, size.y) # Set the `pivot_offset` to the bottom-left corner.
	rotation_degrees = 90.0
	scale = Vector2(0.1, 0.1)
	show()
	
	if tween:
		tween.kill()
	
	tween = create_tween()
	tween.set_parallel(true)
	tween.set_ease(Tween.EASE_OUT)
	tween.set_trans(Tween.TRANS_ELASTIC)
	tween.tween_property(self, "rotation_degrees", 0.0, duration)
	tween.tween_property(self, "scale", Vector2.ONE, duration)
	tween.set_trans(Tween.TRANS_LINEAR)
	tween.tween_property(says_ui, "visible_ratio", 1.0, reading_time)


## Animation tween for the `Next()` function.
#func TweenNext(duration: float):
#	says_ui.visible_ratio = 0.0
#	scale = Vector2(0.8, 0.7)
#	if tween:
#		tween.kill()
#
#	tween = create_tween()
#	tween.set_parallel(true)
#	tween.set_ease(Tween.EASE_OUT)
#	tween.set_trans(Tween.TRANS_LINEAR)
#	tween.tween_property(says_ui, "visible_ratio", 1.0, quickly)
#	tween.set_trans(Tween.TRANS_ELASTIC)
#	tween.tween_property(self, "scale", Vector2.ONE, duration)
#	tween.set_parallel(false)


## Animation tween for the `Close()` function.
#func TweenClose(duration: float):
#	if tween:
#		tween.kill()
#
#	tween = create_tween()
#	tween.set_ease(Tween.EASE_IN)
#	tween.set_trans(Tween.TRANS_CIRC)
#	tween.tween_property(self, "scale", Vector2(0.1, 0.1), duration)
#	tween.set_parallel(false)
#	tween.tween_callback(hide)
