extends Button
class_name NodeDealer

const SPEAKER_NODE = preload("res://Nodes/SpeakerNode/SpeakerNode.tscn")

@onready var speech_balloon: SpeechBalloon = $SpeechBalloon
@onready var extras_panel: ExtrasPanel = $ExtrasPanel
@onready var play_button: Button = $PlayButton
@onready var name_tag: Label = $NameTagPanel/NameTag

@export var transition_type: Tween.TransitionType = Tween.TRANS_BACK
@export var transition_speed: float = 0.3

## Link back to the`NodeGraph` base class.
var dialog_tree: DialogTree 
var current_speaker: SpeakerData = SpeakerData.new()


func _ready():
	play_button.pressed.connect(OnPlayPressed)
	extras_panel.node_dealer = self
	speech_balloon.node_dealer = self


## Returns `SpeakerData` from the first node attached to the "PlayNode".
func OnPlayPressed():
	dialog_tree.playhead_name = "PlayNode"
	var first_data: SpeakerData = dialog_tree.GetSpeakerData(0)
	print(first_data)
	if first_data:
		name_tag.text = first_data.who
		speech_balloon.Open(first_data)


func Deal():
	var speaker_node = SPEAKER_NODE.instantiate()
	speaker_node.position_offset = Vector2(0, get_viewport().size.y) + dialog_tree.graph_edit.scroll_offset
	dialog_tree.graph_edit.add_child(speaker_node)
	speaker_node.dialog_tree = dialog_tree
	
	var final_position = Vector2(randi_range(0, get_viewport().size.x / 2), randi_range(0, get_viewport().size.y / 2)) + dialog_tree.graph_edit.scroll_offset
	var tween = create_tween()
	tween.set_parallel(true)
	tween.set_ease(Tween.EASE_OUT)
	tween.set_trans(transition_type)
	tween.tween_property(speaker_node, "position_offset", final_position, transition_speed)
