# Node Dealer

An NPC who deals nodes to the node graph. The `Node Dealer` provides the UI location to store unlockables and achievements.


## Important Nodes (NodeDealer.tscn)

- Button (base class): trigger Deal().
- ExtrasButton: trigger OpenExtras().
- ExtrasUI: a selection of properties, achievements, etc.
- SpeechBalloon: our friendly NPC gives advice and information.


## Properties & Functions

- Deal(): add a new `Speaker Node` to the the `Dialog Tree`.
- FriendlyConversation(): pops up a `Speech Balloon` with a conversation.
