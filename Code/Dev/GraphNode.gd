extends GraphNode

func _ready():
	resize_request.connect(_on_resize_request)

func _on_resize_request(new_size: Vector2):
	size = new_size
