# The "window_drag.gd" script will turn any Control node into
# a title bar that will be able to drag the window.
extends Control

var is_dragging: bool
var pressed_offset: Vector2i


func _ready():
	# Signal hooks.
	gui_input.connect(OnGUIInput)

func _process(delta):
	if is_dragging:
		get_window().position = DisplayServer.mouse_get_position() - pressed_offset

func _input(event):
	if event is InputEventMouseButton:
		if not event.pressed:
			is_dragging = false

# Signal callbacks.
func OnGUIInput(event):
	if event is InputEventMouseButton:
		if event.pressed:
			is_dragging = true
			pressed_offset = get_viewport().get_mouse_position()
