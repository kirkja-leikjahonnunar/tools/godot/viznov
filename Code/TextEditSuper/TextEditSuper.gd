extends TextEdit
class_name TextEditSuper

@export var line_min: int = 1
@export var line_max: int = 3

var padding: int = 4 # TODO: Get from theme.
var line_height: int


func _ready():
	text_changed.connect(OnTextChanged)
	line_height = get_line_height() + padding
#	print(line_height)
#	custom_minimum_size.y = line_height


func OnTextChanged():
	print("Typing.")
#	if text.count("\n") < line_max:
#		custom_minimum_size.y = line_max * line_height
#	else:
#		scroll_fit_content_height = false
