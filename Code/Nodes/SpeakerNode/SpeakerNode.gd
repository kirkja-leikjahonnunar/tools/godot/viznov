extends GraphNode
class_name SpeakerNode

const CHOICE_UI = preload("res://ChoiceUI/ChoiceUI.tscn")

@onready var who_ui = $HBox/WhoUI
@onready var tint_button = $HBox/TintButton
@onready var says_ui = $SaysUI
@onready var add_choice_button = $AddChoiceButton

var dialog_tree: DialogTree # Spawner ref.
var choice_suffix: int = 1


func _ready():
	# `SpeakerNode` Behaviors.
	resize_request.connect(OnResizeRequest)
	close_request.connect(OnCloseRequest)
	node_selected.connect(OnSelected)
	node_deselected.connect(OnDeselected)
	dragged.connect(OnDragged)
	show_close = false
	
	# Inputs.
	add_choice_button.pressed.connect(OnAddChoice)
	tint_button.color_changed.connect(OnTintChanged)


## Reads the node Controls and refreshes the data structure.
func GetData():
	var data = SpeakerData.new()
	data.who = who_ui.text
	data.says = says_ui.text
	data.choices.clear()
	
	for child in get_children():
		if child is ChoiceUI:
			data.choices.append(child.text)
	
	return data


# Signal Callbacks.
func OnSelected():
	show_close = true


func OnDeselected():
	show_close = false


func OnDragged(from: Vector2, to: Vector2):
	print("From: ", from)
	print("To: ", to)


#func OnWhoEdited():
#	#dialog_tree.UpdateSpeakersList(speaker_in.text)
##	data.who = who_ui.text
##	print(data.who)
#	Data()


#func OnSaysEdited():
##	data.says = says_ui.text
##	print(data.says)
#	Data()


func OnTintChanged(new_color: Color):
	set_slot_color_left(0, new_color)
#	modulate = new_color
	var frame = theme.get_stylebox("GraphNode/styles/frame", "StyleBoxFlat")
	print("FRAME: ", frame.border_color)
	frame.border_color = new_color
#	print("Color!!!! ", frame.border_color)
	
	var color = theme.get_color("GraphNode/colors/resizer_color", "")
	print(color)
#	color = Color.RED
#	theme.add_color_override("GraphNode/colors/resizer_color")
#	var sb = theme.get_stylebox("GraphNode/styles/frame", "StyleBoxFlat")
	
	# TODO: Brighter colors! Or tap into the Theme.


func OnResizeRequest(new_size: Vector2):
	size = new_size


func OnCloseRequest():
	dialog_tree.OnDeleted([self.name])


func OnAddChoice():
	var choice_ui = CHOICE_UI.instantiate()
#	choice_ui.text_changed.connect(OnChoiceChanged)
	add_child(choice_ui)
	move_child(choice_ui, -2)
	choice_ui.speaker_node = self
	choice_suffix += 1
	choice_ui.placeholder_text = "Choice " + str(choice_suffix)
	set_slot(get_child_count() - 2, false, 0, Color.BLACK, true, 0, Color.RED, null, null, true)


#func UpdateSlots():
#	# Turn off all the slots.
#	for i in choices_node.get_child_count():
#		set_slot(i, false, 0, Color.BLACK, false, 0, Color.RED, null, null, true)
#
#	# Turn on all slots except the top 2 and the last node.
#	var child_index: int = 2
#	while child_index < get_child_count() - 2:
#		child_index += 1
#		set_slot(child_index, false, 0, Color.BLACK, true, 0, Color.RED, null, null, true)
##	TODO: print("A".to_int()) Choice D, Choice E, Choice F.


func RemoveChoice(choice_ui: ChoiceUI):
	choice_ui.queue_free()
#	UpdateSlots()
