extends Node
class_name SpeakerData ## A data class used by `SpeakerNodes`.

var who: String = "wEIrdo" ## Who is speaking?
var says: String = "Saying what?" ## The body of the text, where text reveal animation, formatting, and effects are possible.
var choices: Array = ["Close"] ## The list of options for each choice in the SpeakerNode.
var tint: Color = Color.BLACK ## A Color that will be used to tint SpeakerNodes, their 
