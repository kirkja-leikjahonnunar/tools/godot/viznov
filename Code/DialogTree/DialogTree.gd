extends Control
class_name DialogTree

@onready var graph_edit = $GraphEdit
@onready var node_dealer = $NodeDealer
@onready var speakers_ui = $Speakers/SpeakersUI
@onready var snap_button = $Controls/SnapButton
@onready var reset_zoom_button = $Controls/ResetZoomButton
@onready var play_node = $GraphEdit/PlayNode

var playhead_name: String = "PlayNode"
var selected_nodes: Array[GraphNode]


func _ready():
	# Toolbar.
	reset_zoom_button.pressed.connect(OnResetZoomPressed)
	snap_button.pressed.connect(OnSnapPressed)
	
	# Graph Edit Signals.
	graph_edit.connection_request.connect(OnConnected)
	graph_edit.disconnection_request.connect(OnDisconnected)
	graph_edit.delete_nodes_request.connect(OnDeleted)
	graph_edit.copy_nodes_request.connect(OnCopied)
	graph_edit.paste_nodes_request.connect(OnPasted)
	graph_edit.duplicate_nodes_request.connect(OnDuplicated)
	graph_edit.node_selected.connect(OnSelected)
	graph_edit.node_deselected.connect(OnDeselected)
	
	# Node Dealer.
	node_dealer.pressed.connect(OnDealerPressed)
	node_dealer.dialog_tree = self
	
	# FIXME: Load from file when we have a little more info.
	Persistent.Load()
	


#func _process(delta):
#
## TODO: Add the ability to backspace delete? If no text field has focus?
#	if Input.is_action_just_pressed("delete_node"):
#		var names: PackedStringArray
#		for node in selected_nodes:
#			names.append(node.name)
#
#		graph_edit.delete_nodes_request.emit(names)


func OnSnapPressed():
	graph_edit.use_snap = !graph_edit.use_snap


func OnResetZoomPressed():
	graph_edit.zoom = 1.0


## Look through all the nodes on the graph_edit and refresh the UI.
func UpdateSpeakersList(speaker_name: String):
	var speaker_list: Array[SpeakerData]
	
	for child in graph_edit.get_children():
		if child is SpeakerNode:
#			# HACK: RETURN HERE !!!!!!!!!!!!!!
			pass #if child.speaker_in
#
#	speakers_ui.text = ""
#	for speaker in speakers:
#		if speaker.who == speaker_name:
#			pass
#		else:
#			speakers_ui.text += "[color=" + speaker.tint.to_html(false) + "]" + speaker.who + "[/color]\n"
#		print("Tint: ", speaker.tint)
#	speakers_ui.text += "- " + speaker_name
#	var result = speakers.find(text_edit.text)
#	if result != -1:
#		print("Found some")
		
	# Look through the list and see if we need to add it.


func GetSpeakerData(choice_index: int) -> SpeakerData:
	var connections = graph_edit.get_connection_list()
	for connection in connections:
		if connection.from == playhead_name:
			if connection.from_port == choice_index:
				playhead_name = connection.to
				if playhead_name:
					var current_node: SpeakerNode = graph_edit.get_node(playhead_name)
					if current_node:
						return current_node.GetData()
				else:
					print_debug("`playhead_name` is null.")
	
	return null



# Signal callbacks.
func OnDealerPressed():
	node_dealer.Deal()


func OnConnected(from_node, from_port, to_node, to_port):
	print("OnConnectRequested: ", from_node, ", ",from_port, ", ",to_node, ", ", to_port)
	var error = graph_edit.connect_node(from_node, from_port, to_node, to_port)
	if error:
		printerr("Not `OK`. Connection failure.")


func OnDisconnected(from_node, from_port, to_node, to_port):
	print("OnDisconnectRequested: ", from_node, ", ",from_port, ", ",to_node, ", ", to_port)
	graph_edit.disconnect_node(from_node, from_port, to_node, to_port)


func OnSelected(node: GraphNode):
	selected_nodes.append(node)
	print(selected_nodes)


func OnDeselected(node: GraphNode):
	selected_nodes.erase(node)
	print(selected_nodes)


# TODO: Hook up the copy and paste functionallity.
var copied_nodes: Array
func OnDuplicated():
	OnCopied()
	OnPasted()

func OnCopied():
	copied_nodes = selected_nodes.duplicate()
	print("COPYING: ", selected_nodes, "\nTO: ", copied_nodes)
	
	



func OnPasted():
	print("SELECTED: ", selected_nodes)
	print("PASTING: ", copied_nodes)
	
	for node in copied_nodes:
		node.selected = false
		var new_node = node.duplicate()
		graph_edit.add_child(new_node)
#		new_node.position = node.position
		new_node.selected = true


func OnDeleted(node_names: PackedStringArray):
	
	# `node_names` are Strings and must be converted to nodes manually.
	for node_name in node_names:
		print(node_name)
		if node_name != "PlayNode":
			var node = graph_edit.get_node(node_name)
			if node:
				# FIXME: Inconsistant `selected_nodes` array maniulation.
				# Erase all the selections from 
				selected_nodes.erase(node)
				node.queue_free()
			else:
				print("Node was null?")
